package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "strconv"
    "strings"
)

const (
    OPERATION_INCREMENT = "inc"
    OPERATION_DECREMENT = "dec"
)

const (
    OPERATOR_GT  = ">"
    OPERATOR_GTE = ">="
    OPERATOR_LT  = "<"
    OPERATOR_LTE = "<="
    OPERATOR_EQ  = "=="
    OPERATOR_NEQ = "!="
)

type Instruction struct {
    Register  string
    Operation string
    Value     int
    Condition *Condition
}

type Condition struct {
    Lhs      string
    Rhs      int
    Operator string
}

func (condition *Condition) Evaluate(registerValueMap map[string]int) bool {
    switch condition.Operator {
    case OPERATOR_GT:
        return registerValueMap[condition.Lhs] > condition.Rhs
    case OPERATOR_GTE:
        return registerValueMap[condition.Lhs] >= condition.Rhs
    case OPERATOR_LT:
        return registerValueMap[condition.Lhs] < condition.Rhs
    case OPERATOR_LTE:
        return registerValueMap[condition.Lhs] <= condition.Rhs
    case OPERATOR_EQ:
        return registerValueMap[condition.Lhs] == condition.Rhs
    case OPERATOR_NEQ:
        return registerValueMap[condition.Lhs] != condition.Rhs
    default:
        log.Fatal("Unsupported condition operator: " + condition.Operator)
    }

    return false;
}

func parseConditionString(condition string) *Condition {
    segments := strings.Split(condition, " ")
    if len(segments) != 3 {
        log.Fatal("Invalid number of segments (" + strconv.Itoa(len(segments)) + ") for condition: " + condition)
    }
    rhs, _ := strconv.Atoi(segments[2])
    return &Condition{
        Lhs:      segments[0],
        Rhs:      rhs,
        Operator: segments[1],
    }
}

func parseInput(input string) []*Instruction {
    instructionsStrings := strings.Split(input, "\n")
    var instructions []*Instruction
    for _, instructionString := range instructionsStrings {
        segments := strings.Split(instructionString, " ")
        value, _ := strconv.Atoi(segments[2])
        instructions = append(instructions, &Instruction{
            Register:  segments[0],
            Operation: segments[1],
            Value:     value,
            Condition: parseConditionString(strings.Join(segments[4:], " ")),
        })
    }

    return instructions
}

func initializeRegisters(instructions []*Instruction) map[string]int {
    registersValueMap := make(map[string]int)
    for _, instruction := range instructions {
        registersValueMap[instruction.Register] = 0
    }

    return registersValueMap
}

func processInstructions(instructions []*Instruction, registerValueMap map[string]int, registerHighestValueMap map[string]int) {
    for _, instruction := range instructions {
        if instruction.Condition.Evaluate(registerValueMap) {
            switch instruction.Operation {
            case OPERATION_INCREMENT:
                registerValueMap[instruction.Register] += instruction.Value
                break
            case OPERATION_DECREMENT:
                registerValueMap[instruction.Register] -= instruction.Value
                break
            }
            if registerValueMap[instruction.Register] > registerHighestValueMap[instruction.Register] {
                registerHighestValueMap[instruction.Register] = registerValueMap[instruction.Register]
            }
        }
    }
}

func findLargestValueInRegisters(registerValueMap map[string]int) int {
    largestValue := 0
    for _, value := range registerValueMap {
        if value > largestValue {
            largestValue = value
        }
    }

    return largestValue
}

func main() {
    instructions := parseInput(util.MustReadDayInput(2017, 8))
    registerValueMap := initializeRegisters(instructions)
    registerHighestValueMap := initializeRegisters(instructions)
    processInstructions(instructions, registerValueMap, registerHighestValueMap)
    largestRegisterValue := findLargestValueInRegisters(registerValueMap)
    largestEverRegisterValue := findLargestValueInRegisters(registerHighestValueMap)
    fmt.Println("Largest register value: " + strconv.Itoa(largestRegisterValue))
    fmt.Println("Largest ever register value: " + strconv.Itoa(largestEverRegisterValue))
}

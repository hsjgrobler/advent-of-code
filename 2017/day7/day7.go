package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "regexp"
    "strconv"
    "strings"
)

type Program struct {
    Name          string
    Weight        int
    TotalWeight   int
    ChildrenNames []string
    ParentName    string
}

func parseInput(input string) map[string]*Program {
    programPattern := `^(?P<name>[a-z]+)\s\((?P<weight>\d+)\)(\s->\s(?P<childrenNames>([a-z]+(,\s)?)+))?$`
    programRegex, err := regexp.Compile(programPattern)
    groupNames := programRegex.SubexpNames()
    groupNamesIndexMap := map[string]int{}
    for index, name := range groupNames {
        if name == "" {
            continue
        }
        groupNamesIndexMap[name] = index
    }
    if err != nil {
        log.Fatal(err)
    }
    programs := make(map[string]*Program)
    for _, programString := range strings.Split(input, "\n") {
        matches := programRegex.FindAllStringSubmatch(programString, -1)[0]
        name := matches[groupNamesIndexMap["name"]]
        weight, err := strconv.Atoi(matches[groupNamesIndexMap["weight"]])
        if err != nil {
            log.Fatal(err)
        }
        childrenNames := strings.Split(matches[groupNamesIndexMap["childrenNames"]], ", ")
        if matches[groupNamesIndexMap["childrenNames"]] == "" {
            childrenNames = make([]string, 0)
        }
        programs[name] = &Program{
            Name:          name,
            Weight:        weight,
            ChildrenNames: childrenNames,
        }
    }

    return programs
}

func linkParents(programs map[string]*Program) {
    for _, program := range programs {
        for _, childName := range program.ChildrenNames {
            childProgram := programs[childName]
            childProgram.ParentName = program.Name
        }
    }
}

func findRootProgramName(programs map[string]*Program) string {
    for _, program := range programs {
        if program.ParentName == "" {
            return program.Name
        }
    }

    return ""
}

func calculateTotalWeight(programName string, programs map[string]*Program) {
    program := programs[programName]
    program.TotalWeight = program.Weight
    for _, childName := range program.ChildrenNames {
        calculateTotalWeight(childName, programs)
        program.TotalWeight += programs[childName].TotalWeight
    }
}

func findRootChildWithIncorrectTotalWeight(programName string, programs map[string]*Program) string {
    program := programs[programName]
    incorrectChildName := determineIncorrectChild(program.ChildrenNames, programs)

    if incorrectChildName == "" {
        return programName
    }

    return findRootChildWithIncorrectTotalWeight(incorrectChildName, programs)
}

func determineIncorrectChild(childrenNames []string, programs map[string]*Program) string {
    if len(childrenNames) <= 2 {
        log.Fatal("Cannot reliably determine the incorrect child when there are 2 or less children")
    }

    childTotalWeightCountMap := make(map[int]int)
    possibleIncorrectChildren := make(map[int]string)

    for _, childName := range childrenNames {
        childWeight := programs[childName].TotalWeight
        _, exists := childTotalWeightCountMap[childWeight]
        if !exists {
            childTotalWeightCountMap[childWeight] = 1
            possibleIncorrectChildren[childWeight] = childName
        } else {
            childTotalWeightCountMap[childWeight]++
        }
    }

    if len(childTotalWeightCountMap) > 2 {
        fmt.Println(childTotalWeightCountMap)
        log.Fatal("Found more than 2 different total weights. Cannot reliably determine incorrect child.")
    }

    incorrectChildName := ""
    if len(childTotalWeightCountMap) == 2 {
        for totalWeight, count := range childTotalWeightCountMap {
            if count == 1 {
                incorrectChildName = possibleIncorrectChildren[totalWeight]
            }
        }
    }

    return incorrectChildName
}

func findFirstSiblingWithCorrectWeight(programName string, programs map[string]*Program) string {
    for _, siblingName := range programs[programs[programName].ParentName].ChildrenNames {
        if siblingName == programName {
            continue
        }

        return siblingName
    }

    return ""
}

func printRelationships(programs map[string]*Program) {
    for name, program := range programs {
        fmt.Println(program.ParentName + " -> " + name + " -> " + strings.Join(program.ChildrenNames, ", "))
    }
}

func printWeights(programs map[string]*Program) {
    for name, program := range programs {
        childrenWeightsString := ""
        for _, childName := range program.ChildrenNames {
            childrenWeightsString += childName + " [" + strconv.Itoa(programs[childName].Weight) + "/" + strconv.Itoa(programs[childName].TotalWeight) + "] "
        }
        fmt.Println(name + " [" + strconv.Itoa(program.Weight) + "/" + strconv.Itoa(program.TotalWeight) + "] -> " + childrenWeightsString)
    }
}

func main() {
    programs := parseInput(util.MustReadDayInput(2017, 7))
    linkParents(programs)
    rootProgramName := findRootProgramName(programs)
    fmt.Println("Root program name: " + rootProgramName)
    calculateTotalWeight(rootProgramName, programs)
    incorrectTotalWeightChildName := findRootChildWithIncorrectTotalWeight(rootProgramName, programs)
    correctTotalWeight := programs[findFirstSiblingWithCorrectWeight(incorrectTotalWeightChildName, programs)].TotalWeight
    fmt.Println("Program with incorrect weight: " + incorrectTotalWeightChildName + "(" + strconv.Itoa(programs[incorrectTotalWeightChildName].TotalWeight) + ")");
    fmt.Println("Correct total weight: " + strconv.Itoa(correctTotalWeight));
    totalWeightDifference := correctTotalWeight - programs[incorrectTotalWeightChildName].TotalWeight
    newWeight := programs[incorrectTotalWeightChildName].Weight + totalWeightDifference
    fmt.Println("Incorrect total weight: " + strconv.Itoa(programs[incorrectTotalWeightChildName].TotalWeight))
    fmt.Println("Weight: " + strconv.Itoa(programs[incorrectTotalWeightChildName].Weight))
    fmt.Println("Weight required to balance " + incorrectTotalWeightChildName + ": " + strconv.Itoa(newWeight))
    fmt.Println("Weight difference: " + strconv.Itoa(totalWeightDifference))
}

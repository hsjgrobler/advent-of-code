package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "sort"
    "strconv"
    "strings"
)

var inputs = []string{
    "aa bb cc dd ee",  //valid
    "aa bb cc dd aa",  //invalid
    "aa bb cc dd aaa", //valid
    util.MustReadDayInput(2017, 4),
    "abcde fghij",              //valid
    "abcde xyz ecdab",          //invalid
    "a ab abc abd abf abj",     //valid
    "iiii oiii ooii oooi oooo", //valid
    "oiii ioii iioi iiio",      //invalid
}

const SECURITY_VERSION_1 = 1
const SECURITY_VERSION_2 = 2

func main() {
    part1Input := inputs[3]
    part2Input := inputs[3]
    part1PassPhrases := strings.Split(part1Input, "\n")
    part2PassPhrases := strings.Split(part2Input, "\n")
    fmt.Println("Number of valid passphrases for security version 1: " + strconv.Itoa(getNumberOfValidPassPhrases(part1PassPhrases, SECURITY_VERSION_1)))
    fmt.Println("Number of valid passphrases for security version 2: " + strconv.Itoa(getNumberOfValidPassPhrases(part2PassPhrases, SECURITY_VERSION_2)))
}

func getNumberOfValidPassPhrases(passPhrases []string, securityVersion int) int {
    validCount := 0
    for _, passPhrase := range passPhrases {
        if passPhraseIsValid(passPhrase, securityVersion) {
            validCount++;
        }
    }

    return validCount
}

func passPhraseIsValid(passPhrase string, securityVersion int) bool {
    switch securityVersion {
    case SECURITY_VERSION_1:
        return !phraseContainsDuplicateWords(passPhrase)
    case SECURITY_VERSION_2:
        return !phraseContainsAnagrams(passPhrase)
    default:
        log.Fatal("Unsupported security version: " + strconv.Itoa(securityVersion))
    }
    return false
}

func phraseContainsAnagrams(phrase string) bool {
    words := strings.Split(phrase, " ");
    for i := 0; i < len(words)-1; i++ {
        for j := i + 1; j < len(words); j++ {
            if wordIsAnagramOf(words[i], words[j]) {
                return true
            }
        }
    }

    return false
}

func getSortedCharactersInAlphabeticalOrder(word string) string {
    characters := strings.Split(word, "")
    sort.Strings(characters)
    return strings.Join(characters, "")
}

func wordIsAnagramOf(wordA string, wordB string) bool {
    if len(wordA) != len(wordB) {
        return false
    }

    wordASorted := getSortedCharactersInAlphabeticalOrder(wordA)
    wordBSorted := getSortedCharactersInAlphabeticalOrder(wordB)

    for i := 0; i < len(wordASorted); i++ {
        if wordASorted[i] != wordBSorted[i] {
            return false
        }
    }

    return true
}

func phraseContainsDuplicateWords(phrase string) bool {
    words := strings.Split(phrase, " ");
    usedWordsMap := map[string]bool{}
    for _, word := range words {
        _, ok := usedWordsMap[word]
        if ok {
            return true
        }
        usedWordsMap[word] = true
    }
    return false
}

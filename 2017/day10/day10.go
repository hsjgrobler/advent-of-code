package main

import (
    "encoding/hex"
    "fmt"
    "log"
    "strconv"
)

var part1inputs = [][]int{
    {3, 4, 1, 5},
    {147, 37, 249, 1, 31, 2, 226, 0, 161, 71, 254, 243, 183, 255, 30, 70}, //37230
}

var part2inputs = []string{
    "",
    "AoC 2017",
    "1,2,3",
    "1,2,4",
    "147,37,249,1,31,2,226,0,161,71,254,243,183,255,30,70", //
}

var listLengths = []int{
    5,
    256,
}

var STANDARD_LENTH_SUFFIXES = []int{17, 31, 73, 47, 23}

func populateList(list []int) []int {
    for i := 0; i < len(list); i++ {
        list[i] = i
    }

    return list
}

func generateHash(list, lengths []int, rounds int) []int {
    skip := 0
    position := 0
    for round := 1; round <= rounds; round++ {
        for _, length := range lengths {
            // Reverse
            for count := 0; count < length/2; count++ {
                index := position + count
                reverseIndex := position + ((length - 1) - count)
                if index >= len(list) {
                    index = index - (len(list) * (index / len(list)))
                }
                if reverseIndex >= len(list) {
                    reverseIndex = reverseIndex - (len(list) * (reverseIndex / len(list)))
                }

                tmp := list[index]
                list[index] = list[reverseIndex]
                list[reverseIndex] = tmp
            }
            position += length + skip
            skip++

            if position >= len(list) {
                position = position - (len(list) * (position / len(list)))
            }
        }
    }

    return list
}

func convertInputToASCII(input string) []int {
    converted := make([]int, len(input))
    for index, char := range input {
        converted[index] = int(char)
    }

    return converted
}

func generateDenseHash(sparseHash []int, blockSize int) []uint8 {
    if len(sparseHash)%blockSize != 0 {
        log.Fatal("Cannot evenly divide hash of length" + strconv.Itoa(len(sparseHash)) + " into chunks of " + strconv.Itoa(blockSize))
    }

    var chunks [][]int
    for i := 0; i < len(sparseHash); i += blockSize {
        chunks = append(chunks, sparseHash[i:i+blockSize])
    }

    var denseHash = make([]uint8, len(chunks))
    for j := 0; j < len(chunks); j++ {
        chunk := chunks[j]
        xor := chunk[0]
        for i := 1; i < len(chunk); i++ {
            xor ^= chunk[i]
        }
        denseHash[j] = uint8(xor)
    }

    return denseHash
}

func denseHashAsHex(denseHash []uint8) string {
    denseHashHex := ""
    for _, number := range denseHash {
        denseHashHex += hex.EncodeToString([]byte{byte(number)})
    }

    return denseHashHex
}

func main() {
    list := make([]int, listLengths[1])
    list = populateList(list)

    part1list := make([]int, len(list))
    copy(part1list, list)
    part1input := make([]int, len(part1inputs[1]))
    copy(part1input, part1inputs[1])
    part1Hash := generateHash(part1list, part1input, 1)
    fmt.Println("Part 1 Hash: ")
    fmt.Println(part1Hash)
    fmt.Println("Product of first two elements of hash: " + strconv.Itoa(part1Hash[0]*part1Hash[1]))

    part2list := make([]int, len(list))
    copy(part2list, list)
    part2input := make([]int, len(part2inputs[4]))
    copy(part2input, convertInputToASCII(part2inputs[4]))
    part2input = append(part2input, STANDARD_LENTH_SUFFIXES...)
    part2SparseHash := generateHash(part2list, part2input, 64)
    fmt.Println("Part 2 Sparse Hash: ")
    fmt.Println(part2SparseHash)
    denseHash := generateDenseHash(part2SparseHash, 16)
    fmt.Println("Part 2 Dense Hash: ")
    fmt.Println(denseHash)
    fmt.Println("Part 2 Knot Hash (hex dense hash): " + denseHashAsHex(denseHash))
}

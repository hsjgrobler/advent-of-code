package main

import (
    "errors"
    "fmt"
    "strconv"
)

var inputs = []int{
    1,
    12,
    23,
    1024,
    325489,
}

const (
    RING_SIDE_RIGHT_VALUE  = 0
    RING_SIDE_TOP_VALUE    = 1
    RING_SIDE_LEFT_VALUE   = 2
    RING_SIDE_BOTTOM_VALUE = 3

    RING_SIDE_RIGHT  = "right"
    RING_SIDE_TOP    = "top"
    RING_SIDE_LEFT   = "left"
    RING_SIDE_BOTTOM = "bottom"
)

// INCOMPLETE
func main() {
    var level int
    var start int
    var blocksPerSide int
    var sideOfRing string
    level, start = findRing(1)
    blocksPerSide = findBlocksPerSideOfRing(1, level)
    sideOfRing, _ = findSideOfRing(1, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(1)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(2)
    blocksPerSide = findBlocksPerSideOfRing(2, level)
    sideOfRing, _ = findSideOfRing(2, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(2)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(5)
    blocksPerSide = findBlocksPerSideOfRing(5, level)
    sideOfRing, _ = findSideOfRing(5, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(5)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(9)
    blocksPerSide = findBlocksPerSideOfRing(9, level)
    sideOfRing, _ = findSideOfRing(9, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(9)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(10)
    blocksPerSide = findBlocksPerSideOfRing(10, level)
    sideOfRing, _ = findSideOfRing(10, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(10)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(17)
    blocksPerSide = findBlocksPerSideOfRing(17, level)
    sideOfRing, _ = findSideOfRing(17, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(17)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(24)
    blocksPerSide = findBlocksPerSideOfRing(24, level)
    sideOfRing, _ = findSideOfRing(24, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(24)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(25)
    blocksPerSide = findBlocksPerSideOfRing(25, level)
    sideOfRing, _ = findSideOfRing(25, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(25)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(26)
    blocksPerSide = findBlocksPerSideOfRing(26, level)
    sideOfRing, _ = findSideOfRing(26, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(26)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
    level, start = findRing(27)
    blocksPerSide = findBlocksPerSideOfRing(27, level)
    sideOfRing, _ = findSideOfRing(27, start, blocksPerSide)
    fmt.Println("Input "+strconv.Itoa(27)+" Ring level: "+strconv.Itoa(level), ", Ring Start: "+strconv.Itoa(start))
    fmt.Println("Blocks per side  " + strconv.Itoa(blocksPerSide))
    fmt.Println("Side of ring: " + sideOfRing)
    fmt.Println("-----------------------------------------")
}

func findOffset() {

}

func isHorizontal(side string) bool {
    return side == RING_SIDE_TOP || side == RING_SIDE_BOTTOM
}

func isVertical(side string) bool {
    return side == RING_SIDE_LEFT || side == RING_SIDE_RIGHT
}

func findSideOfRing(input int, ringStart int, blocksPerSide int) (string, error) {
    side := (input - ringStart) / blocksPerSide
    switch side {
    case RING_SIDE_RIGHT_VALUE:
        return RING_SIDE_RIGHT, nil
    case RING_SIDE_TOP_VALUE:
        return RING_SIDE_TOP, nil
    case RING_SIDE_LEFT_VALUE:
        return RING_SIDE_LEFT, nil
    case RING_SIDE_BOTTOM_VALUE:
        return RING_SIDE_BOTTOM, nil
    default:
        return "", errors.New("Unsupported side: " + strconv.Itoa(side))
    }
}

func findBlocksPerSideOfRing(input int, level int) int {
    return (2 * level) - 1;
}

func findRing(input int) (level int, start int) {
    level = 1;
    start = 1;
    lastStart := 1;
    for ; input > start; level ++ {
        lastStart = start
        start += (level * 8)
    }

    return level, lastStart + 1
}

package main

import (
    "fmt"
    "strconv"
)

const (
    COLLISION_DETECTION_STRATEGY_V1 = 1
    COLLISION_DETECTION_STRATEGY_V2 = 2
)

var inputs = [][]int{
    {0, 2, 7, 0},
    {4, 1, 15, 12, 0, 9, 9, 5, 5, 8, 7, 3, 14, 5, 12, 3},
}

func main() {
    input := inputs[1]
    var v1Input = make([]int, len(input));
    var v2Input = make([]int, len(input));
    copy(v1Input, input)
    copy(v2Input, input)
    firstCollisionCycles := findCollisionCycleOfBanksRedistribution(v1Input, COLLISION_DETECTION_STRATEGY_V1)
    secondCollisionCycles := findCollisionCycleOfBanksRedistribution(v2Input, COLLISION_DETECTION_STRATEGY_V2)
    fmt.Println("Found first collision on cycle: " + strconv.Itoa(firstCollisionCycles))
    fmt.Println("Found second collision v2 on cycle: " + strconv.Itoa(secondCollisionCycles))
    fmt.Println("Cycles between first and second collision: " + strconv.Itoa(secondCollisionCycles-firstCollisionCycles))
}

func findCollisionCycleOfBanksRedistribution(banks []int, collisionDetectionStrategyVersion int) int {
    cycle := 0
    found := false
    seenPermutations := map[string]bool{}
    for ; !found; cycle++ {
        found = redistributeBlocks(banks, seenPermutations, collisionDetectionStrategyVersion)
    }

    return cycle
}

func findBankWithMostBlocks(banks []int) int {
    mostBlocksIndex := 0
    mostBlocks := banks[mostBlocksIndex]
    for i := 1; i < len(banks); i++ {
        if banks[i] > mostBlocks {
            mostBlocks = banks[i]
            mostBlocksIndex = i
        }
    }

    return mostBlocksIndex
}

func findBankToReDistribute(banks []int) int {
    return findBankWithMostBlocks(banks)
}

func redistributeBlocks(banks []int, seenPermutations map[string]bool, collisionDetectionStrategyVersion int) bool {
    redistributeBankIndex := findBankToReDistribute(banks)
    redistributeBankBlocks := banks[redistributeBankIndex]
    count := 0
    banks[redistributeBankIndex] = 0

    for i := redistributeBankIndex + 1; count < redistributeBankBlocks; i++ {
        if i == len(banks) {
            i = 0
        }
        banks[i]++
        count++
    }

    stamp := fmt.Sprintf("%v", banks)
    secondTime, exists := seenPermutations[stamp]
    if exists {
        switch collisionDetectionStrategyVersion {
        case COLLISION_DETECTION_STRATEGY_V1:
            return true
        case COLLISION_DETECTION_STRATEGY_V2:
            if secondTime {
                return true
            }
            seenPermutations[stamp] = true
        }
    } else {
        seenPermutations[stamp] = false
    }

    return false
}

package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "strings"
)

func parseInput(input string) map[int][]int {
    pipesMap := make(map[int][]int)
    for _, pipeString := range strings.Split(input, "\n") {
        parts := strings.Split(pipeString, " <-> ")
        programString := parts[0]
        connectedProgramsString := parts[1]
        var childPrograms []int
        for _, childProgram := range strings.Split(connectedProgramsString, ", ") {
            intVal, _ := strconv.Atoi(childProgram)
            childPrograms = append(childPrograms, intVal)
        }
        parentProgram, _ := strconv.Atoi(programString)
        pipesMap[parentProgram] = childPrograms
    }
    return pipesMap
}

func findNumberOfConnectedPrograms(program int, pipesMap map[int][]int, groupedPrograms map[int]int) int {
    connectedPrograms := pipesMap[program]
    connectedProgramsCount := 0
    for _, connectedProgram := range connectedPrograms {
        _, exists := groupedPrograms[connectedProgram]
        if !exists {
            connectedProgramsCount++
            groupedPrograms[connectedProgram] = program
            connectedProgramsCount += findNumberOfConnectedPrograms(connectedProgram, pipesMap, groupedPrograms)
        }
    }

    return connectedProgramsCount
}

func groupConnectedPrograms(program int, pipesMap map[int][]int, groupedPrograms map[int]int, group []int) []int {
    groupedPrograms[program] = program
    group = append(group, program)
    connectedPrograms := pipesMap[program]
    for _, connectedProgram := range connectedPrograms {
        _, exists := groupedPrograms[connectedProgram]
        if !exists {
            group = groupConnectedPrograms(connectedProgram, pipesMap, groupedPrograms, group)
        }
    }

    return group
}

func groupAllConnectedPrograms(pipesMap map[int][]int) [][]int {
    var groups [][]int
    groupedPrograms := make(map[int]int)
    for program, _ := range pipesMap {
        if _, exists := groupedPrograms[program]; exists {
            continue
        }
        groups = append(groups, groupConnectedPrograms(program, pipesMap, groupedPrograms, []int{}))
    }
    return groups
}

func main() {
    part1pipesMap := parseInput(util.MustReadDayInput(2017, 12))
    part2pipesMap := parseInput(util.MustReadDayInput(2017, 12))
    entryProgram := 0
    groupedPrograms := make(map[int]int)
    group := groupConnectedPrograms(entryProgram, part1pipesMap, groupedPrograms, []int{})
    fmt.Println("Number of programs connected through program " + strconv.Itoa(entryProgram) + ": " + strconv.Itoa(len(group)))
    groups := groupAllConnectedPrograms(part2pipesMap)
    fmt.Println("Number of groups: " + strconv.Itoa(len(groups)))
}

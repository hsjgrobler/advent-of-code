package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "strings"
)

func parseInput(input string) (layerRangeMap map[int]int) {
    layerRangeMap = make(map[int]int)
    lines := strings.Split(input, "\n")
    for _, line := range lines {
        keyValuePair := strings.Split(line, ": ")
        layer, _ := strconv.Atoi(keyValuePair[0])
        scannerRange, _ := strconv.Atoi(keyValuePair[1])
        layerRangeMap[layer] = scannerRange
    }

    return
}

func buildFirewall(layerRangeMap map[int]int) (firewall [][]bool) {
    maxLayer := 0
    for layer, _ := range layerRangeMap {
        if layer > maxLayer {
            maxLayer = layer
        }
    }

    firewall = make([][]bool, maxLayer+1)
    for layer, scannerRange := range layerRangeMap {
        firewall[layer] = make([]bool, scannerRange)
        firewall[layer][0] = true
    }
    return
}

func findMaxRangeOfFirewall(firewall [][]bool) int {
    maxScannerRange := 0
    for _, scannerRange := range firewall {
        if len(scannerRange) > maxScannerRange {
            maxScannerRange = len(scannerRange)
        }
    }

    return maxScannerRange
}

func advanceTime(picoSecond int, packetLayer int, firewall [][]bool) (caught bool, severity int) {
    severity = 0
    caught = isCaught(packetLayer, picoSecond, firewall)
    if caught {
        severity += packetLayer * len(firewall[packetLayer])
    }
    return
}

func isCaught(packetLayer int, picoSecond int, firewall [][]bool) bool {
    if len(firewall[packetLayer]) == 0 {
        return false
    }
    return picoSecond%((len(firewall[packetLayer])*2)-2) == 0
}

func sendPacketThroughFirewall(firewall [][]bool, picoSecond int, returnOnCaught bool) (caught bool, tripSeverity int) {
    caught = false
    packetLayer := 0
    tripSeverity = 0

    for packetLayer < len(firewall) {
        isCaught, severity := advanceTime(picoSecond, packetLayer, firewall)
        tripSeverity += severity
        if !caught && isCaught {
            caught = true
            if returnOnCaught {
                return
            }
        }
        picoSecond++
        packetLayer++
    }
    return
}

func bruteForceFindShortestDelayForTripWithoutBeingCaught(layerRangeMap map[int]int) (delay int) {
    caught := true
    firewall := buildFirewall(layerRangeMap)
    for delay = 0; caught; delay++ {
        caught, _ = sendPacketThroughFirewall(firewall, delay, true)
    }
    return delay - 1
}

func main() {
    layerRangeMap := parseInput(util.MustReadDayInput(2017, 13))
    part1firewall := buildFirewall(layerRangeMap)
    _, tripSeverity := sendPacketThroughFirewall(part1firewall, 0, false)
    fmt.Println("Packet sent through firewall. Severity of trip: " + strconv.Itoa(tripSeverity))

    fmt.Println("Finding shortest delay for trip without being caught...")
    delay := bruteForceFindShortestDelayForTripWithoutBeingCaught(layerRangeMap)
    fmt.Println("Shortest delay to avoid being caught: " + strconv.Itoa(delay))
}

package main

import (
    "fmt"
    "github.com/golang-collections/collections/stack"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "regexp"
    "strconv"
    "strings"
)

var garbage = []string{
    `<>`,
    `<random characters>`,
    `<<<<>`,
    `<{!>}>`,
    `<!!>`,
    `<!!!>>`,
    `<{o"i!a,<{i<a>`,
}

var groups = []string{
    `{}`,
    `{{{}}}`,
    `{{},{}}`,
    `{{{},{},{{}}}}`,
    `{<{},{},{{}}>}`,
    `{<a>,<a>,<a>,<a>}`,
    `{{<a>},{<a>},{<a>},{<a>}}`,
    `{{<!>},{<!>},{<!>},{<a>}}`,
}

var inputs = []string{
    `{}`,                            //1
    `{{{}}}`,                        //6
    `{{},{}}`,                       //5
    `{{{},{},{{}}}}`,                //16
    `{<a>,<a>,<a>,<a>}`,             //1
    `{{<ab>},{<ab>},{<ab>},{<ab>}}`, //9
    `{{<!!>},{<!!>},{<!!>},{<!!>}}`, //9
    `{{<a!>},{<a!>},{<a!>},{<ab>}}`, //3
    util.MustReadDayInput(2017, 9),
}

func parseInput(input string) []string {
    return strings.Split(input, "\n");
}

func stripOutGarbage(stream string) (string, int) {
    cleanedStream := stripOutIgnored(stream)
    garbagePattern := `<.*?>`
    garbageCharacterCount := 0
    garbageRegex, err := regexp.Compile(garbagePattern)
    if err != nil {
        log.Fatal(err)
    }
    matches := garbageRegex.FindAllString(cleanedStream, -1)
    for _, match := range matches {
        garbageCharacterCount += len(match) - 2
    }
    return garbageRegex.ReplaceAllString(cleanedStream, ""), garbageCharacterCount
}

func stripOutIgnored(stream string) string {
    cleanedStream := ""
    for i := 0; i < len(stream); i++ {
        char := string(stream[i])
        if char != "!" {
            cleanedStream += char
            continue
        }
        // Skip the next character
        i++
    }
    return cleanedStream
}

func calculateStreamGroupsScore(stream string) int {
    score := 0
    groups := stack.New()
    for i := 0; i < len(stream); i++ {
        char := string(stream[i])
        switch char {
        case "{":
            groups.Push(groups.Len() + 1)
            break
        case "}":
            score += groups.Peek().(int)
            groups.Pop()
            break
        default:
            continue
        }
    }

    return score
}

func calculateTotalScoreOfStreams(streams []string) (int, int) {
    totalScore := 0
    totalGarbageCharactersCount := 0
    for _, stream := range streams {
        cleanedStream, garbageCharacterCount := stripOutGarbage(stream)
        totalGarbageCharactersCount += garbageCharacterCount
        totalScore += calculateStreamGroupsScore(cleanedStream)
    }
    return totalScore, totalGarbageCharactersCount
}

func main() {
    var input = inputs[8]
    streams := parseInput(input)
    totalScore, totalGarbageCharactersCount := calculateTotalScoreOfStreams(streams)
    fmt.Println("Total score: " + strconv.Itoa(totalScore))
    fmt.Println("Total garbage characters removed: " + strconv.Itoa(totalGarbageCharactersCount))
}

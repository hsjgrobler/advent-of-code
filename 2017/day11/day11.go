package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "math"
    "strconv"
    "strings"
)

func parseInput(input string) []string {
    return strings.Split(input, ",")
}

var inputs = [][]string{
    {"ne", "ne", "ne"},
    {"ne", "ne", "sw", "sw"},
    {"ne", "ne", "s", "s"},
    {"se", "sw", "se", "sw", "sw"},
    parseInput(util.MustReadDayInput(2017, 11)),
}

const (
    DIR_N  = "n"
    DIR_NE = "ne"
    DIR_SE = "se"
    DIR_S  = "s"
    DIR_SW = "sw"
    DIR_NW = "nw"
)

var DIRECTIONS = []string{
    DIR_N, DIR_NE, DIR_SE, DIR_S, DIR_SW, DIR_NW,
}

type Coordinate struct {
    X int
    Y int
    Z int
}

func (coordinate Coordinate) serialize() string {
    return strconv.Itoa(coordinate.X) + "," + strconv.Itoa(coordinate.Y) + "," + strconv.Itoa(coordinate.Z)
}

// Y = +/-
// X = -|+
// Z = +\-
var DIR_COORDS_MAP = map[string]Coordinate{
    DIR_N:  Coordinate{0, 1, -1},
    DIR_NE: Coordinate{1, 0, -1},
    DIR_SE: Coordinate{1, -1, 0},
    DIR_S:  Coordinate{0, -1, 1},
    DIR_SW: Coordinate{-1, 0, 1},
    DIR_NW: Coordinate{-1, 1, 0},
}

type HexTile struct {
    //DistanceFromCenter int
    Coordinate      Coordinate
    DistanceToStart int
    StepDirection   string
    StepCount       int
}

func addDirectionToCoordinate(direction string, coordinate Coordinate) Coordinate {
    mappedCoordinate := DIR_COORDS_MAP[direction];
    newCoordinate := Coordinate{
        coordinate.X + mappedCoordinate.X,
        coordinate.Y + mappedCoordinate.Y,
        coordinate.Z + mappedCoordinate.Z,
    }

    return newCoordinate
}

func buildGrid(steps []string) (map[string]HexTile, map[int]string) {
    stepCountCoordinateStringMap := make(map[int]string)
    startCoordinate := Coordinate{0, 0, 0}
    startTile := HexTile{
        Coordinate:      startCoordinate,
        DistanceToStart: 0,
        StepDirection:   "",
        StepCount:       0,
    }
    stepCountCoordinateStringMap[0] = startCoordinate.serialize()
    grid := make(map[string]HexTile)
    grid[startCoordinate.serialize()] = startTile
    previousTile := startTile
    count := 1

    for _, step := range steps {
        coordinate := addDirectionToCoordinate(step, previousTile.Coordinate)
        tile := HexTile{
            Coordinate:    coordinate,
            StepDirection: step,
            StepCount:     count,
        }
        closestNeighbour := findNeighbourClosestToStart(tile, grid)
        tile.DistanceToStart = closestNeighbour.DistanceToStart + 1
        grid[coordinate.serialize()] = tile
        stepCountCoordinateStringMap[count] = coordinate.serialize()
        previousTile = tile
        count++
    }

    return grid, stepCountCoordinateStringMap
}

func getNeighboursForTile(tile HexTile, grid map[string]HexTile) map[string]HexTile {
    neighbours := make(map[string]HexTile)
    for _, direction := range DIRECTIONS {
        neighbour, exists := grid[addDirectionToCoordinate(direction, tile.Coordinate).serialize()]
        if !exists {
            continue
        }
        neighbours[direction] = neighbour
    }

    return neighbours
}

func findNeighbourClosestToStart(tile HexTile, grid map[string]HexTile) HexTile {
    neighbours := getNeighboursForTile(tile, grid)
    closestNeighbourDirection := ""
    closestDistanceToStart := 99999999
    for direction, neighbour := range neighbours {
        if neighbour.DistanceToStart < closestDistanceToStart {
            closestDistanceToStart = neighbour.DistanceToStart
            closestNeighbourDirection = direction
        }
    }

    if closestNeighbourDirection == "" {
        log.Fatal("Could not find a closest neighbour")
    }

    return neighbours[closestNeighbourDirection]
}

func distanceBetweenTiles(tileA HexTile, tileB HexTile) int {
    return int(math.Abs(float64(tileA.Coordinate.X-tileB.Coordinate.X))+math.Abs(float64(tileA.Coordinate.Y-tileB.Coordinate.Y))+math.Abs(float64(tileA.Coordinate.Z-tileB.Coordinate.Z))) / 2
}

func findFurthestDistanceFromStart(grid map[string]HexTile) int {
    furthestDistance := 0
    for _, tile := range grid {
        distanceBetweenTileAndStart := distanceBetweenTiles(tile, HexTile{Coordinate: Coordinate{0, 0, 0,}})
        if distanceBetweenTileAndStart > furthestDistance {
            furthestDistance = distanceBetweenTileAndStart
        }
    }

    return furthestDistance
}

func main() {
    input := inputs[4]
    grid, stepCountCoordinateMap := buildGrid(input)
    fmt.Println("Length of shortest path from last step to starting point: " + strconv.Itoa(distanceBetweenTiles(grid[stepCountCoordinateMap[len(input)]], grid[stepCountCoordinateMap[0]])))
    furthestDistanceFromStart := findFurthestDistanceFromStart(grid)
    fmt.Println("Furthest distance away from start: " + strconv.Itoa(furthestDistanceFromStart))
}

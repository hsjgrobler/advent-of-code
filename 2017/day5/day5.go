package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "log"
    "strconv"
    "strings"
)

const STEP_INCREMENT_STRATEGY_V1 = 1
const STEP_INCREMENT_STRATEGY_V2 = 2

func getInputFileContents() []int {
    instructions := strings.Split(util.MustReadDayInput(2017, 5), "\n")
    convertedInstructions := make([]int, len(instructions))
    for i := 0; i < len(instructions); i++ {
        convertedInstruction, err := strconv.Atoi(instructions[i])
        if err != nil {
            log.Fatal(err)
        }
        convertedInstructions[i] = convertedInstruction
    }

    return convertedInstructions
}

var inputs [][]int = [][]int{
    {0, 3, 0, 1, -3},
    getInputFileContents(),
}

func main() {
    v1Input := make([]int, len(inputs[1]))
    v2Input := make([]int, len(inputs[1]))
    copy(v1Input, inputs[1])
    copy(v2Input, inputs[1])
    fmt.Println("Steps to escape using increment strategy v1: " + strconv.Itoa(findNumberOfStepsToEscape(v1Input, STEP_INCREMENT_STRATEGY_V1)))
    fmt.Println("Steps to escape using increment strategy v2: " + strconv.Itoa(findNumberOfStepsToEscape(v2Input, STEP_INCREMENT_STRATEGY_V2)))
}

func findNumberOfStepsToEscape(instructions []int, incrementStrategyVersion int) int {
    index := 0
    steps := 0
    for ; index >= 0 && index < len(instructions); index = getNextInstructionIndex(index, instructions, incrementStrategyVersion) {
        steps++
    }

    return steps
}

func getNextInstructionIndex(index int, instructions []int, incrementStrategyVersion int) int {
    instruction := instructions[index]
    defer func() {
        switch incrementStrategyVersion {
        case STEP_INCREMENT_STRATEGY_V1:
            instructions[index]++
            break
        case STEP_INCREMENT_STRATEGY_V2:
            if instruction >= 3 {
                instructions[index]--
            } else {
                instructions[index]++
            }
            break
        }
    }()
    return index + instruction
}

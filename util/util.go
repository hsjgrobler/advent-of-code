package util

import (
    "io/ioutil"
    "os"
    "strconv"
    "strings"
)

const projectRootFolder = "advent-of-code"

func getWorkingDirectory() string {
    dir, err := os.Getwd()
    if err != nil {
        panic(err.Error())
    }
    return dir
}

func isAtProjectRoot(path string) bool {
    segments := strings.Split(path, "/")
    if segments[len(segments)-1] == projectRootFolder {
        return true
    }
    return false
}

func isBelowProjectRoot(path string) bool {
    segments := strings.Split(path, "/")
    for _, segment := range segments[:len(segments)-1] {
        if segment == projectRootFolder {
            return true
        }
    }

    return false
}

func goUpOneLevel(path string) string {
    segments := strings.Split(path, "/")
    return strings.Join(segments[:len(segments)-1], "/")
}

func ReadFileFromProjectRoot(relativePath string) string {
    relativePath = strings.TrimLeft(relativePath, "./")

    wd := getWorkingDirectory()
    if !isAtProjectRoot(wd) && !isBelowProjectRoot(wd) {
        panic("working directory is not at or below project root")
    }

    for isBelowProjectRoot(wd) {
        wd = goUpOneLevel(wd)
    }

    bytes, err := ioutil.ReadFile(wd + "/" + relativePath)
    if err != nil {
        panic(err.Error())
    }

    return string(bytes)
}

func MustReadDayInput(year int, day int) string {
    return ReadFileFromProjectRoot(strconv.Itoa(year) + "/day" + strconv.Itoa(day) + "/input.txt")
}

func MustReadDayExample(year int, day int) string {
    return ReadFileFromProjectRoot(strconv.Itoa(year) + "/day" + strconv.Itoa(day) + "/example.txt")
}

func MustReadDayExampleLine(year int, day int, line int) string {
    return strings.Split(ReadFileFromProjectRoot(strconv.Itoa(year)+"/day"+strconv.Itoa(day)+"/example.txt"), "\n")[line-1]
}

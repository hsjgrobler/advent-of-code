package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "strings"
)

func main() {
    input := util.MustReadDayInput(2018, 1)
    frequencies := extractFrequenciesFromInput(input)
    sum := calculateSumOfFrequencies(frequencies)
    fmt.Println("Sum of frequencies:", sum)
    firstRepeatingFreq := findFirstRepeatingFrequency(frequencies)
    fmt.Println("First repeating frequency:", firstRepeatingFreq)
}

func extractFrequenciesFromInput(input string) (frequencies []int) {
    parts := strings.Split(input, "\n")
    for _, part := range parts {
        intVal, err := strconv.ParseInt(part, 10, 32)
        if err != nil {
            panic(err.Error())
        }
        frequencies = append(frequencies, int(intVal))
    }

    return
}

func calculateSumOfFrequencies(frequencies []int) (sum int) {
    for _, frequency := range frequencies {
        sum += frequency
    }
    return
}

func findFirstRepeatingFrequency(frequencies []int) int {
    sumMap := make(map[int]struct{}, len(frequencies))
    var sum int
    for {
        for _, frequency := range frequencies {
            sum += frequency
            if _, exists := sumMap[sum]; exists {
                return sum
            }
            sumMap[sum] = struct{}{}
        }
    }
}

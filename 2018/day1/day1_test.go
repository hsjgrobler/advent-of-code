package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 536
const AnswerPart2 = 75108

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 1)
    frequencies := extractFrequenciesFromInput(input)

    sum := calculateSumOfFrequencies(frequencies)
    if sum != AnswerPart1 {
        t.Errorf("expected sum of frequencies to be %d, got %d", AnswerPart1, sum)
    }

    firstRepeatingFreq := findFirstRepeatingFrequency(frequencies)
    if firstRepeatingFreq != AnswerPart2 {
        t.Errorf("expected first repeating frequency to be %d, got %d", AnswerPart2, firstRepeatingFreq)
    }
}

// @todo Add tests for other functions

package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "strings"
    "testing"
)

const AnswerPart1 = "ACHOQRXSEKUGMYIWDZLNBFTJVP"

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 7)
    steps := extractStepsFromInput(input)
    order := getCorrectStepsOrder(steps)
    orderString := strings.Join(order, "")
    if orderString != AnswerPart1 {
        t.Errorf("expected correct step order to be %s, got %s", AnswerPart1, orderString)
    }
}

package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "regexp"
    "strings"
)

const StepPattern = `Step (\w+) must be finished before step (\w+) can begin.`

const BaseSecondsToCompleteTask = 60

type Step struct {
    ID            string
    DependsOn     []string
    Completed     bool
    Started       bool
    StartedSecond int
}

func main() {
    input := util.MustReadDayInput(2018, 7)
    steps := extractStepsFromInput(input)
    order := getCorrectStepsOrder(steps)
    fmt.Println("Correct steps order:", strings.Join(order, ""))
}

func completeSteps(steps map[string]*Step, workers int) (secondsTaken int) {
    availableWorkers := workers
    availableSteps := findIndependentSteps(steps)
    for second := 0; true; second++ {
        for _, step := range steps {
            if !step.Completed && step.Started {
                if second-step.StartedSecond == calculateSecondsToCompleteStep(step) {
                    completeStep(step.ID, steps)
                }
            }
        }

        var remainingSteps []*Step
        for _, step := range availableSteps {
            if availableWorkers == 0 {
                remainingSteps = append(remainingSteps, step)
                continue
            }
            availableWorkers--
            step.Started = true
            step.StartedSecond = second
        }
        availableSteps = remainingSteps
    }

    return
}

func findInProgressSteps(steps map[string]*Step) (inProgress []*Step) {
    for _, step := range steps {
        if step.Started == true {
            inProgress = append(inProgress, step)
        }
    }

    return
}

func calculateSecondsToCompleteStep(step *Step) int {
    return int(BaseSecondsToCompleteTask + step.ID[0])
}

func getCorrectStepsOrder(steps map[string]*Step) []string {
    order := make([]string, 0, len(steps))
    rootStep := determineNextStep(findIndependentSteps(steps))
    for step := rootStep; step != nil; {
        order = append(order, step.ID)
        step = completeStep(step.ID, steps)
    }

    return order
}

func completeStep(id string, steps map[string]*Step) (nextStep *Step) {
    steps[id].Completed = true
    possibleNextSteps := make([]*Step, 0, 1)
    for _, step := range steps {
        if step.Completed {
            continue
        }
        var remainingDependentStepIds []string
        if len(step.DependsOn) > 1 {
            remainingDependentStepIds = make([]string, 0, len(step.DependsOn)-1)
        }
        for _, dependentStepId := range step.DependsOn {
            if id != dependentStepId {
                remainingDependentStepIds = append(remainingDependentStepIds, dependentStepId)
            }
        }
        step.DependsOn = remainingDependentStepIds
        if len(step.DependsOn) == 0 {
            possibleNextSteps = append(possibleNextSteps, step)
        }
    }

    nextStep = determineNextStep(possibleNextSteps)
    return
}

func determineNextStep(possibleSteps []*Step) (nextStep *Step) {
    if len(possibleSteps) == 0 {
        return nil
    }

    nextStep = possibleSteps[0]
    for _, step := range possibleSteps {
        if strings.Compare(step.ID, nextStep.ID) == -1 {
            nextStep = step
        }
    }

    return
}

func findIndependentSteps(steps map[string]*Step) (independentSteps []*Step) {
    for _, step := range steps {
        if len(step.DependsOn) == 0 {
            independentSteps = append(independentSteps, step)
        }
    }

    return
}

func extractStepsFromInput(input string) map[string]*Step {
    compiledPattern, err := regexp.Compile(StepPattern)
    if err != nil {
        panic(err.Error())
    }
    stepStrings := strings.Split(input, "\n")
    stepMap := make(map[string]*Step)
    for _, stepString := range stepStrings {
        matches := compiledPattern.FindStringSubmatch(stepString)
        id := matches[2]
        dependsOn := matches[1]
        if _, exists := stepMap[id]; !exists {
            stepMap[id] = &Step{
                ID:        id,
                DependsOn: []string{dependsOn},
            }
        } else {
            stepMap[id].DependsOn = append(stepMap[id].DependsOn, dependsOn)
        }
        if _, exists := stepMap[dependsOn]; !exists {
            stepMap[dependsOn] = &Step{
                ID:        dependsOn,
                DependsOn: nil,
            }
        }
    }

    return stepMap
}

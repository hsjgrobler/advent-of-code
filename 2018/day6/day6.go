package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "math"
    "strconv"
    "strings"
)

type Coordinates struct {
    X int
    Y int
}

type Location struct {
    Name        string
    Coordinates Coordinates
}

type FiniteGrid struct {
    CoordinatesMap map[Coordinates]*Location
    TopLeft        Coordinates
    BottomRight    Coordinates
}

const MaxTotalDistanceExample = 32
const MaxTotalDistanceInput = 10000

func main() {
    input := util.MustReadDayInput(2018, 6)
    locations := extractLocationsFromInput(input)
    topLeft, bottomRight := findCornerCoordinates(locations)
    grid := newFiniteGrid(topLeft, bottomRight)
    grid = mapClosestLocations(locations, grid)
    //drawGrid(locations, grid, topLeft, bottomRight)
    areas := calculateLocationAreas(locations, grid)
    location, area := findLargestNonInfiniteArea(areas)
    fmt.Println("Location with the largest non-infinite area:", location.Name, area)
    proximalLocations := findCoordinatesWithinProximityToLocations(locations, MaxTotalDistanceInput, grid)
    fmt.Println("Number of coordinates under threshold distance to all locations:", len(proximalLocations))
}

func findCoordinatesWithinProximityToLocations(locations []Location, proximity int, grid FiniteGrid) (found []Coordinates) {
    for x := grid.TopLeft.X; x <= grid.BottomRight.X; x++ {
        for y := grid.TopLeft.Y; y <= grid.BottomRight.Y; y++ {
            currentCoordinates := Coordinates{X: x, Y: y}
            distance := calculateTotalDistanceToLocations(currentCoordinates, locations)
            if distance < proximity {
                found = append(found, currentCoordinates)
            }
        }
    }

    return
}

func calculateTotalDistanceToLocations(coordinates Coordinates, locations []Location) (totalDistance int) {
    for _, location := range locations {
        totalDistance += calculateManhattanDistance(coordinates, location.Coordinates)
    }
    return
}

func findLargestNonInfiniteArea(areas map[Location]float64) (largestLocation Location, largestArea int) {
    for location, area := range areas {
        if math.IsInf(area, 0) {
            continue
        }
        if int(area) > largestArea {
            largestArea = int(area)
            largestLocation = location
        }
    }

    if largestArea == 0.0 {
        panic("unable to find largest non-infinite area")
    }

    return
}

func calculateLocationAreas(locations []Location, grid FiniteGrid) (areas map[Location]float64) {
    areas = make(map[Location]float64, len(locations))
    for x := grid.TopLeft.X; x <= grid.BottomRight.X; x++ {
        for y := grid.TopLeft.Y; y <= grid.BottomRight.Y; y++ {
            currentCoordinates := Coordinates{X: x, Y: y}
            currentLocation := grid.CoordinatesMap[currentCoordinates]
            if currentLocation == nil {
                continue
            }

            // Check if is infinite
            if currentCoordinates.X == grid.TopLeft.X ||
                currentCoordinates.Y == grid.TopLeft.Y ||
                currentCoordinates.X == grid.BottomRight.X ||
                currentCoordinates.Y == grid.BottomRight.Y {
                areas[*currentLocation] = math.Inf(0)
            }

            for _, location := range locations {
                if *currentLocation == location {
                    if math.IsInf(areas[location], 0) {
                        continue
                    }
                    areas[location]++
                }
            }
        }
    }

    return areas
}

func calculateManhattanDistance(coordinatesA Coordinates, coordinatesB Coordinates) (distance int) {
    return int(math.Abs(float64(coordinatesA.X-coordinatesB.X)) + math.Abs(float64(coordinatesA.Y-coordinatesB.Y)))
}

func mapClosestLocations(locations []Location, grid FiniteGrid) FiniteGrid {
    for x := grid.TopLeft.X; x <= grid.BottomRight.X; x++ {
        for y := grid.TopLeft.Y; y <= grid.BottomRight.Y; y++ {
            smallestDistance := math.MaxInt32
            distanceLocationsMap := make(map[int][]*Location)
            for _, location := range locations {
                distance := calculateManhattanDistance(
                    Coordinates{X: x, Y: y},
                    location.Coordinates,
                )
                tmp := location
                distanceLocationsMap[distance] = append(distanceLocationsMap[distance], &tmp)
                if distance < smallestDistance {
                    smallestDistance = distance
                }
            }

            if len(distanceLocationsMap[smallestDistance]) == 1 {
                grid.CoordinatesMap[Coordinates{X: x, Y: y}] = distanceLocationsMap[smallestDistance][0]
            }
        }
    }

    return grid
}

func newFiniteGrid(topLeft Coordinates, bottomRight Coordinates) (grid FiniteGrid) {
    grid = FiniteGrid{
        CoordinatesMap: make(map[Coordinates]*Location),
        TopLeft:        topLeft,
        BottomRight:    bottomRight,
    }

    // Fill Grid
    for x := topLeft.X; x <= bottomRight.X; x++ {
        for y := topLeft.Y; y <= bottomRight.Y; y++ {
            grid.CoordinatesMap[Coordinates{X: x, Y: y}] = nil
        }
    }

    return
}

func drawGrid(locations []Location, grid FiniteGrid) {
    for y := grid.TopLeft.Y; y <= grid.BottomRight.Y; y++ {
        for x := grid.TopLeft.X; x <= grid.BottomRight.X; x++ {
            currentCoordinates := Coordinates{X: x, Y: y}
            isLocation := false
            for _, location := range locations {
                if location.Coordinates == currentCoordinates {
                    fmt.Print(location.Name)
                    isLocation = true
                    break
                }
            }
            if !isLocation {
                if grid.CoordinatesMap[currentCoordinates] != nil {
                    fmt.Print(strings.ToLower(grid.CoordinatesMap[currentCoordinates].Name))
                } else {
                    fmt.Print(".")
                }
            }
        }
        fmt.Println()
    }
}

func findCornerCoordinates(locations []Location) (topLeft Coordinates, bottomRight Coordinates) {
    lowestX := math.MaxInt32
    lowestY := math.MaxInt32
    highestX := 0
    highestY := 0

    for _, location := range locations {
        if location.Coordinates.X < lowestX {
            lowestX = location.Coordinates.X
        }
        if location.Coordinates.X > highestX {
            highestX = location.Coordinates.X
        }
        if location.Coordinates.Y < lowestY {
            lowestY = location.Coordinates.Y
        }
        if location.Coordinates.Y > highestY {
            highestY = location.Coordinates.Y
        }
    }

    topLeft = Coordinates{
        X: lowestX,
        Y: lowestY,
    }
    bottomRight = Coordinates{
        X: highestX,
        Y: highestY,
    }

    return
}

func generateLocationName(index int) string {
    divisions := index / 26
    remainder := index - (divisions * 26)
    name := strings.Repeat("A", divisions) + string('A'+remainder)
    return name
}

func extractLocationsFromInput(input string) []Location {
    locationStrings := strings.Split(input, "\n")
    locations := make([]Location, 0, len(locationStrings))

    for index, locationString := range locationStrings {
        parts := strings.Split(locationString, ", ")
        x, err := strconv.ParseInt(parts[0], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        y, err := strconv.ParseInt(parts[1], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        locations = append(locations, Location{
            Name: generateLocationName(index),
            Coordinates: Coordinates{
                X: int(x),
                Y: int(y),
            },
        })
    }

    return locations
}

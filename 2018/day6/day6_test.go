package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 4771
const AnswerPart2 = 39149

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 6)
    locations := extractLocationsFromInput(input)
    topLeft, bottomRight := findCornerCoordinates(locations)
    grid := newFiniteGrid(topLeft, bottomRight)

    grid = mapClosestLocations(locations, grid)
    areas := calculateLocationAreas(locations, grid)
    _, area := findLargestNonInfiniteArea(areas)
    if area != AnswerPart1 {
        t.Errorf("expected largest non-infinite location area to be %d, got %d", AnswerPart1, area)
    }

    proximalLocations := findCoordinatesWithinProximityToLocations(locations, MaxTotalDistanceInput, grid)
    if len(proximalLocations) != AnswerPart2 {
        t.Errorf("expected count of coordinates under threshold distance to all locations to be %d, got %d", AnswerPart2, len(proximalLocations))
    }
}

package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 98005
const AnswerPart2 = "331"

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 3)
    rectangles := extractRectanglesFromInput(input)
    _, overlaps := mapRectangles(rectangles)

    if len(overlaps) != AnswerPart1 {
        t.Errorf("expected overlapping rectangles count to be %d, got %d", AnswerPart1, len(overlaps))
    }

    nonOverlappingRectangle := findNonOverlappingRectangle(rectangles, overlaps)

    if nonOverlappingRectangle.ID != AnswerPart2 {
        t.Errorf("expected non-overlapping rectangle id to be %s, got %s", AnswerPart2, nonOverlappingRectangle.ID)
    }
}

// @todo Add tests for other functions
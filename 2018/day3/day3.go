package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "regexp"
    "strconv"
    "strings"
)

const RectanglePattern = `#(\d+) @ (\d+),(\d+): (\d+)x(\d+)`

type Rectangle struct {
    ID     string
    X      int
    Y      int
    Width  int
    Height int
}

func main() {
    input := util.MustReadDayInput(2018, 3)
    rectangles := extractRectanglesFromInput(input)
    _, overlaps := mapRectangles(rectangles)
    fmt.Println("Overlap count:", len(overlaps))
    nonOverlappingRectangle := findNonOverlappingRectangle(rectangles, overlaps)
    fmt.Println("Non-overlapping rectangle ID:", nonOverlappingRectangle.ID)
}

func extractRectanglesFromInput(input string) (rectangles []Rectangle) {
    compiledPattern, err := regexp.Compile(RectanglePattern)
    if err != nil {
        panic(err.Error())
    }
    rectangleStrings := strings.Split(input, "\n")
    rectangles = make([]Rectangle, 0, len(rectangleStrings))

    for _, rectangleString := range rectangleStrings {
        matches := compiledPattern.FindAllStringSubmatch(rectangleString, -1)
        x, err := strconv.ParseInt(matches[0][2], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        y, err := strconv.ParseInt(matches[0][3], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        width, err := strconv.ParseInt(matches[0][4], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        height, err := strconv.ParseInt(matches[0][5], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        rectangles = append(rectangles, Rectangle{
            ID:     matches[0][1],
            X:      int(x),
            Y:      int(y),
            Width:  int(width),
            Height: int(height),
        })
    }

    return
}

func findNonOverlappingRectangle(rectangles []Rectangle, overlaps map[[2]int][]string) Rectangle {
    remainingRectangles := make(map[string]Rectangle, len(rectangles))
    for _, rectangle := range rectangles {
        remainingRectangles[rectangle.ID] = rectangle
    }

    total := 0
    for _, overlapRectangles := range overlaps {
        for _, overlapRectangle := range overlapRectangles {
            total++
            delete(remainingRectangles, overlapRectangle)
        }
    }


    if len(remainingRectangles) != 1 {
        panic("found more or less than 1 remaining rectangle, got: " + strconv.Itoa(len(remainingRectangles)))
    }

    for _, remainingRectangle := range remainingRectangles {
        return remainingRectangle
    }

    return Rectangle{}
}

func mapRectangles(rectangles []Rectangle) (grid map[[2]int]string, overlaps map[[2]int][]string) {
    grid = make(map[[2]int]string)
    overlaps = make(map[[2]int][]string)

    for _, rectangle := range rectangles {
        for x := 0; x < rectangle.Width; x++ {
            for y := 0; y < rectangle.Height; y++ {
                coordinates := [2]int{rectangle.X + x, rectangle.Y + y}
                if _, doesOverlap := grid[coordinates]; doesOverlap {
                    overlaps[coordinates] = append(overlaps[coordinates], grid[coordinates], rectangle.ID)
                } else {
                    grid[coordinates] = rectangle.ID
                }
            }
        }
    }

    return
}

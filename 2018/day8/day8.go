package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "strings"
)

type Node struct {
    ID                 string
    HeaderChildNodeQty int
    HeaderMetaDataQty  int
    ChildNodes         []*Node
    MetaData           []int
}

func main() {
    input := util.MustReadDayInput(2018, 8)
    nodes := extractNodesFromInput(input)
    rootNode, _ := buildTreeFromInput(nodes, 0)
    metaDataSum := sumNodeMetaData(rootNode)
    fmt.Println("Tree meta-data sum:", metaDataSum)
    value := calculateNodeValue(rootNode)
    fmt.Println("Root node value:", value)
}

func generateNodeID(index int) string {
    divisions := index / 26
    remainder := index - (divisions * 26)
    name := strings.Repeat("A", divisions) + string('A'+remainder)
    return name
}

func extractNodesFromInput(input string) []int {
    parts := strings.Split(input, " ")
    intParts := make([]int, 0, len(parts))
    for _, part := range parts {
        intPart, err := strconv.ParseInt(part, 10, 32)
        if err != nil {
            panic(err.Error())
        }
        intParts = append(intParts, int(intPart))
    }

    return intParts
}

func calculateNodeValue(node *Node) int {
    var value int
    if len(node.ChildNodes) == 0 {
        for _, entry := range node.MetaData {
            value += entry
        }
    } else {
        for _, childIndex := range node.MetaData {
            if childIndex == 0 || childIndex > len(node.ChildNodes) {
                continue
            }
            value += calculateNodeValue(node.ChildNodes[childIndex-1])
        }
    }

    return value
}

func sumNodeMetaData(node *Node) int {
    sum := 0

    for _, entry := range node.MetaData {
        sum += entry
    }

    for _, childNode := range node.ChildNodes {
        sum += sumNodeMetaData(childNode)
    }

    return sum
}

func buildTreeFromInput(nodes []int, index int) (node *Node, remainingNodes []int) {
    id := generateNodeID(index)
    childNodeQty := nodes[0]
    metaDataQty := nodes[1]

    remainingNodes = nodes[2:]
    childNodes := make([]*Node, 0, childNodeQty)
    for i := 0; i < childNodeQty; i++ {
        childNode, childRemainingNodes := buildTreeFromInput(remainingNodes, index+i+1)
        remainingNodes = childRemainingNodes
        childNodes = append(childNodes, childNode)
    }

    metaData := make([]int, 0, len(remainingNodes))
    metaData = append(metaData, remainingNodes[:metaDataQty]...)
    remainingNodes = remainingNodes[metaDataQty:]

    node = &Node{
        ID:                 id,
        HeaderChildNodeQty: childNodeQty,
        HeaderMetaDataQty:  metaDataQty,
        ChildNodes:         childNodes,
        MetaData:           metaData,
    }

    return
}

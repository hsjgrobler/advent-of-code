package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 47112
const AnswerPart2 = 28237

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 8)
    nodes := extractNodesFromInput(input)
    rootNode, _ := buildTreeFromInput(nodes, 0)
    metaDataSum := sumNodeMetaData(rootNode)
    if metaDataSum != AnswerPart1 {
        t.Errorf("expected meta-data sum to be %d, got %d", AnswerPart1, metaDataSum)
    }

    value := calculateNodeValue(rootNode)
    if value != AnswerPart2 {
        t.Errorf("expected root value to be %d, got %d", AnswerPart2, value)
    }
}

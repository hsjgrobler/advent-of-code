package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "math"
    "strings"
)

func main() {
    input := util.MustReadDayInput(2018, 2)
    ids := extractIdsFromInput(input)
    twoCountIds := findIdsWithSpecificCharacterCount(ids, 2)
    threeCountIds := findIdsWithSpecificCharacterCount(ids, 3)
    fmt.Println("Checksum:", len(twoCountIds)*len(threeCountIds))
    lhsId, rhsId := findSimilarIdPair(ids)
    fmt.Println("Common letters between box IDs:", findCommonCharacters(lhsId, rhsId))
}

func extractIdsFromInput(input string) (ids []string) {
    ids = strings.Split(input, "\n")
    return
}

func findIdsWithSpecificCharacterCount(ids []string, count int) (foundIds []string) {
    foundMap := make(map[string]struct{})
    for _, id := range ids {
        for _, character := range id {
            characterCount := getCharacterCount(id, character)
            if characterCount == count {
                if _, exists := foundMap[id]; exists {
                    continue
                }
                foundMap[id] = struct{}{}
                foundIds = append(foundIds, id)
            }
        }
    }

    return
}

func getCharacterCount(id string, character rune) int {
    return len(strings.Split(id, string(character))) - 1
}

func findSimilarIdPair(ids []string) (string, string) {
    for i := 0; i < len(ids)-1; i++ {
        id := ids[i]
        for j := i + 1; j < len(ids); j++ {
            nextId := ids[j]
            count := getDifferingCharacterCount(id, nextId)
            if count == 1 {
                return id, nextId
            }
        }
    }

    return "", ""
}

func getDifferingCharacterCount(lhsId string, rhsId string) int {
    differingCount := int(math.Abs(float64(len(lhsId) - len(rhsId))))
    for i := 0; i < len(lhsId) && i < len(rhsId); i++ {
        if lhsId[i] != rhsId[i] {
            differingCount++
        }
    }

    return differingCount
}

func findCommonCharacters(lhsId string, rhsId string) string {
    var commonCharacters string

    for i := 0; i < len(lhsId) && i < len(rhsId); i++ {
        if lhsId[i] != rhsId[i] {
            continue
        }
        commonCharacters += string(lhsId[i])
    }

    return commonCharacters
}

package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 8715
const AnswerPart2 = "fvstwblgqkhpuixdrnevmaycd"

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 2)
    ids := extractIdsFromInput(input)
    twoCountIds := findIdsWithSpecificCharacterCount(ids, 2)
    threeCountIds := findIdsWithSpecificCharacterCount(ids, 3)
    checksum := len(twoCountIds) * len(threeCountIds)

    if checksum != AnswerPart1 {
        t.Errorf("expected checksum to be %d, got %d", AnswerPart1, checksum)
    }

    lhsId, rhsId := findSimilarIdPair(ids)
    commonCharacters := findCommonCharacters(lhsId, rhsId)
    if commonCharacters != AnswerPart2 {
        t.Errorf("expected checksum to be %s, got %s", AnswerPart2, commonCharacters)
    }
}

// @todo Add tests for other functions
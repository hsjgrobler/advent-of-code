package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "regexp"
    "sort"
    "strconv"
    "strings"
    "time"
)

const (
    PatternTimestamp       = `\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\]`
    PatternGuardBeganShift = `Guard #(\d+) begins shift`
    PatternGuardFellAsleep = `falls asleep`
    PatternGuardWokeUp     = `wakes up`
)

const (
    EventTypeGuardBeganShift = "guard_began_shift"
    EventTypeGuardFellAsleep = "guard_fell_asleep"
    EventTypeGuardWokeUp     = "guard_woke_up"
)

type Event struct {
    Type      string
    Timestamp time.Time
    GuardId   string
}

type Period struct {
    From time.Time
    To   time.Time
}

type Schedule struct {
    Asleep []Period
}

func main() {
    input := util.MustReadDayInput(2018, 4)
    events := extractEventsFromInput(input)
    schedules := buildGuardSchedulesFromEvents(events)
    longestSleepingGuard := findLongestSleepingGuard(schedules)
    fmt.Println("Longest sleeping guard:", longestSleepingGuard)
    mostAsleepMinute := findMostAsleepMinute(*schedules[longestSleepingGuard])
    fmt.Println("Most asleep minute:", mostAsleepMinute)
    guard, minute := findMostFrequentlyAsleepGuardByMinute(schedules)
    fmt.Println("Most frequently asleep guard by minute:", guard, minute)
}

func findMostFrequentlyAsleepGuardByMinute(schedules map[string]*Schedule) (guard string, minute int) {
    minuteGuardCount := make(map[string]int, 60)
    for m := 0; m <= 59; m++ {
        for g, schedule := range schedules {
            for _, asleep := range schedule.Asleep {
                if asleep.From.Minute() <= m && m <= asleep.To.Minute() {
                    minuteGuardCount[strconv.Itoa(m)+"|"+g]++
                }
            }
        }
    }

    highestCount := 0
    var highestMinuteGuard string
    for minuteGuard, count := range minuteGuardCount {
        if count > highestCount {
            highestCount = count
            highestMinuteGuard = minuteGuard
        }
    }

    if highestMinuteGuard == "" {
        panic("unable to find most frequently asleep guard by minute")
    }

    parts := strings.Split(highestMinuteGuard, "|")
    minute, err := strconv.Atoi(parts[0])
    if err != nil {
        panic(err.Error())
    }
    guard = parts[1]

    return
}

func findMostAsleepMinute(schedule Schedule) int {
    minuteCounts := make(map[int]int)
    for _, asleep := range schedule.Asleep {
        for i := 0; i <= int(asleep.To.Sub(asleep.From).Minutes()); i++ {
            minute := asleep.From.Minute() + i
            minuteCounts[minute]++
        }
    }

    var highestCount int
    var highestCountMinute int
    for minute, count := range minuteCounts {
        if count > highestCount {
            highestCount = count
            highestCountMinute = minute
        }
    }

    if highestCountMinute == 0 {
        panic("unable to determine most asleep minute")
    }

    return highestCountMinute
}

func findLongestSleepingGuard(schedules map[string]*Schedule) string {
    longest := 0
    var longestSleepingGuard string
    for guard, schedule := range schedules {
        totalSleep := 0
        for _, asleep := range schedule.Asleep {
            totalSleep += int(asleep.To.Sub(asleep.From).Minutes())
        }
        if totalSleep > longest {
            longest = totalSleep
            longestSleepingGuard = guard
        }
    }

    if longestSleepingGuard == "" {
        panic("unable to find longest sleeping guard")
    }

    return longestSleepingGuard
}

func buildGuardSchedulesFromEvents(events []Event) (schedules map[string]*Schedule) {
    schedules = make(map[string]*Schedule)

    var period Period
    for _, event := range events {
        if _, exists := schedules[event.GuardId]; !exists {
            schedules[event.GuardId] = new(Schedule)
        }
        switch event.Type {
        case EventTypeGuardFellAsleep:
            period.From = event.Timestamp
            break
        case EventTypeGuardWokeUp:
            if period.From.IsZero() {
                panic("cannot process guard wake up event when sleep time is not set")
            }
            period.To = event.Timestamp.Add(-time.Minute)
            schedules[event.GuardId].Asleep = append(schedules[event.GuardId].Asleep, period)
            period = Period{}
            break
        }
    }

    return
}

func extractEventsFromInput(input string) (events []Event) {
    eventStrings := strings.Split(input, "\n")
    timestamp, err := regexp.Compile(PatternTimestamp)
    if err != nil {
        panic(err.Error())
    }
    guardBeganShift, err := regexp.Compile(PatternGuardBeganShift)
    if err != nil {
        panic(err.Error())
    }
    guardFellAsleep, err := regexp.Compile(PatternGuardFellAsleep)
    if err != nil {
        panic(err.Error())
    }
    guardWokeUp, err := regexp.Compile(PatternGuardWokeUp)
    if err != nil {
        panic(err.Error())
    }
    var guardId string
    sort.Strings(eventStrings)
    for _, eventString := range eventStrings {
        timestampMatch := timestamp.FindStringSubmatch(eventString)
        eventTimestamp, err := time.Parse("2006-01-02 15:04", timestampMatch[1])
        var eventType string
        if err != nil {
            panic(err.Error())
        }
        switch {
        case guardBeganShift.MatchString(eventString):
            guardMatches := guardBeganShift.FindStringSubmatch(eventString)
            guardId = guardMatches[1]
            eventType = EventTypeGuardBeganShift
            break
        case guardFellAsleep.MatchString(eventString):
            if guardId == "" {
                panic("cannot process event with no active guard: " + eventString)
            }
            eventType = EventTypeGuardFellAsleep
            break
        case guardWokeUp.MatchString(eventString):
            if guardId == "" {
                panic("cannot process event with no active guard: " + eventString)
            }
            eventType = EventTypeGuardWokeUp
            break
        }
        event := Event{
            Type:      eventType,
            Timestamp: eventTimestamp,
            GuardId:   guardId,
        }
        events = append(events, event)
    }

    return
}

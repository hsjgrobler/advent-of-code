package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "testing"
)

const AnswerPart1 = 4716
const AnswerPart2 = 117061

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 4)
    events := extractEventsFromInput(input)
    schedules := buildGuardSchedulesFromEvents(events)

    longestSleepingGuard := findLongestSleepingGuard(schedules)
    mostAsleepMinute := findMostAsleepMinute(*schedules[longestSleepingGuard])

    longestSleepingGuardInt, err := strconv.ParseInt(longestSleepingGuard, 10, 32)
    if err != nil {
        panic(err.Error())
    }

    checksum := int(longestSleepingGuardInt) * mostAsleepMinute
    if checksum != AnswerPart1 {
        t.Errorf("expected longest sleeping guard and most asleep minute checksum to be %d, got %d", AnswerPart1, checksum)
    }

    guard, minute := findMostFrequentlyAsleepGuardByMinute(schedules)

    guardInt, err := strconv.ParseInt(guard, 10, 32)
    if err != nil {
        panic(err.Error())
    }

    checksum = minute * int(guardInt)
    if checksum != AnswerPart2 {
        t.Errorf("expected find frequently asleep guard by minute checksum to be %d, got %d", AnswerPart2, checksum)
    }
}

// @todo Add tests for other functions
package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strings"
    "sync"
)

const AsciiCaseDiff = 32

func main() {
    polymer := util.MustReadDayInput(2018, 5)
    fullyReactedPolymer := fullyReactPolymer(polymer)
    fmt.Println("Reacted polymer:", fullyReactedPolymer)
    fmt.Println("Remaining units:", len(fullyReactedPolymer))
    optimalUnit, length := findOptimalUnitToExclude(polymer)
    fmt.Println("Optimal unit to exclude:", string(optimalUnit))
    fmt.Println("Length of optimally excluded polymer:", length)
}

func findOptimalUnitToExclude(polymer string) (unit rune, length int) {
    results := make(chan struct {
        unit    rune
        polymer string
    }, 26)
    wg := sync.WaitGroup{}
    wg.Add(26)
    for i := 0; i < 26; i++ {
        excludeUnit := 'A' + i
        excludedPolymer := excludeUnitFromPolymer(polymer, rune(excludeUnit))
        go func(excludedPolymer string, unit rune) {
            results <- struct {
                unit    rune
                polymer string
            }{
                unit:    unit,
                polymer: fullyReactPolymer(excludedPolymer),
            }
            wg.Done()
        }(excludedPolymer, rune(excludeUnit))
    }

    wg.Wait()
    close(results)

    length = len(polymer)
    for result := range results {
        if len(result.polymer) < length {
            length = len(result.polymer)
            unit = result.unit
        }
    }

    return
}

func excludeUnitFromPolymer(polymer string, unit rune) string {
    pairUnit := unit + AsciiCaseDiff
    return strings.NewReplacer(string(unit), "", string(pairUnit), "").Replace(polymer)
}

func fullyReactPolymer(polymer string) string {
    var prevPolymer string
    for prevPolymer != polymer {
        prevPolymer = polymer
        polymer = reactPolymer(polymer)
    }

    return polymer
}

func reactPolymer(polymer string) string {
    for i := 0; i < len(polymer)-1; i++ {
        if canReact(rune(polymer[i]), rune(polymer[i+1])) {
            polymer = polymer[:i] + polymer[i+2:]
        }
    }
    return polymer
}

func isUpperCase(unit rune) bool {
    return unit <= 'Z'
}

func isLowerCase(unit rune) bool {
    return unit >= 'a'
}

func canReact(unitA rune, unitB rune) bool {
    return (isLowerCase(unitA) && unitA-AsciiCaseDiff == unitB) || (isUpperCase(unitA) && unitA+AsciiCaseDiff == unitB)
}

package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 9390
const AnswerPart2 = 5898

func TestAnswers(t *testing.T) {
    polymer := util.MustReadDayInput(2018, 5)

    fullyReactedPolymer := fullyReactPolymer(polymer)
    if len(fullyReactedPolymer) != AnswerPart1 {
        t.Errorf("expected length of fully reacted polymer to be %d, got %d", AnswerPart1, len(fullyReactedPolymer))
    }

    _, length := findOptimalUnitToExclude(polymer)
    if length != AnswerPart2 {
        t.Errorf("expected polymer length after removing optimal unit to be %d, got %d", AnswerPart2, length)
    }
}

// @todo Add tests for other functions
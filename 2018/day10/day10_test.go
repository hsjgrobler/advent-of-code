package main

import (
    "bytes"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strings"
    "testing"
)

const AnswerPart1 = `
#####...#####....####...######....##....######..#####...#####.
#....#..#....#..#....#.......#...#..#...#.......#....#..#....#
#....#..#....#..#............#..#....#..#.......#....#..#....#
#....#..#....#..#...........#...#....#..#.......#....#..#....#
#####...#####...#..........#....#....#..#####...#####...#####.
#..#....#....#..#.........#.....######..#.......#.......#.....
#...#...#....#..#........#......#....#..#.......#.......#.....
#...#...#....#..#.......#.......#....#..#.......#.......#.....
#....#..#....#..#....#..#.......#....#..#.......#.......#.....
#....#..#####....####...######..#....#..######..#.......#.....
`
const AnswerPart2 = 10076

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 10)
    points := extractPointsFromInput(input)
    second, messagePoints := findMessage(points)
    buf := bytes.NewBufferString("")
    drawPoints(messagePoints, buf)
    message := buf.String()
    if strings.TrimSpace(message) != strings.TrimSpace(AnswerPart1) {
        t.Errorf("expected message to be \n%s\ngot\n%s", AnswerPart1, message)
    }
    if second != AnswerPart2 {
        t.Errorf("expected seconds taken to be %d, got %d", AnswerPart2, second)
    }
}

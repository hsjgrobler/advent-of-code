package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "io"
    "math"
    "os"
    "regexp"
    "strconv"
    "strings"
)

type Coordinates struct {
    X int
    Y int
}

type Velocity struct {
    XPerSecond int
    YPerSecond int
}

type Point struct {
    Coordinates Coordinates
    Velocity    Velocity
}

const PointPattern = `position=<\s*?(-?\d+),\s*?(-?\d+)> velocity=<\s*?(-?\d+),\s*?(-?\d+)>`

func main() {
    input := util.MustReadDayInput(2018, 10)
    points := extractPointsFromInput(input)
    second, messagePoints := findMessage(points)
    fmt.Println("After", second, "seconds:")
    drawPoints(messagePoints, os.Stdout)
}

func findMessage(points []*Point) (second int, messagePoints []*Point) {
    score := calculatePointsScore(points)
    lowestScore := score
    next := points
    for ; score <= lowestScore; second++ {
        next = calculateNextCoordinates(next)
        score = calculatePointsScore(next)
        if score < lowestScore {
            lowestScore = score
            messagePoints = next
        }
    }
    second--

    return
}

func calculateManhattanDistance(coordinatesA Coordinates, coordinatesB Coordinates) (distance int) {
    return int(math.Abs(float64(coordinatesA.X-coordinatesB.X)) + math.Abs(float64(coordinatesA.Y-coordinatesB.Y)))
}

func calculatePointsScore(points []*Point) (score int) {
    for i := 0; i < len(points)-1; i++ {
        for j := i; j < len(points); j++ {
            point := points[i]
            comparePoint := points[j]
            score += calculateManhattanDistance(point.Coordinates, comparePoint.Coordinates)
        }
    }

    return
}

func drawPoints(points []*Point, writer io.Writer) {
    pointsMap := mapPoints(points)
    topLeft, bottomRight := findCornerCoordinates(points)
    for y := topLeft.Y; y <= bottomRight.Y; y++ {
        for x := topLeft.X; x <= bottomRight.X; x++ {
            currentCoordinates := Coordinates{
                X: x,
                Y: y,
            }
            _, exists := pointsMap[currentCoordinates]
            if exists {
                _, err := fmt.Fprint(writer, "#")
                if err != nil {
                    panic(err.Error())
                }
            } else {
                _, err := fmt.Fprint(writer, ".")
                if err != nil {
                    panic(err.Error())
                }
            }
        }
        _, err := fmt.Fprintln(writer)
        if err != nil {
            panic(err.Error())
        }
    }
}

func mapPoints(points []*Point) (pointsMap map[Coordinates]*Point) {
    pointsMap = make(map[Coordinates]*Point)
    for _, point := range points {
        pointsMap[point.Coordinates] = point
    }

    return pointsMap
}

func calculateNextCoordinates(points []*Point) (next []*Point) {
    next = make([]*Point, 0, len(points))
    for _, point := range points {
        next = append(next, &Point{
            Coordinates: Coordinates{
                X: point.Coordinates.X + point.Velocity.XPerSecond,
                Y: point.Coordinates.Y + point.Velocity.YPerSecond,
            },
            Velocity: point.Velocity,
        })
    }

    return
}

func findCornerCoordinates(points []*Point) (topLeft Coordinates, bottomRight Coordinates) {
    lowestX := math.MaxInt32
    lowestY := math.MaxInt32
    highestX := 0
    highestY := 0

    for _, location := range points {
        if location.Coordinates.X < lowestX {
            lowestX = location.Coordinates.X
        }
        if location.Coordinates.X > highestX {
            highestX = location.Coordinates.X
        }
        if location.Coordinates.Y < lowestY {
            lowestY = location.Coordinates.Y
        }
        if location.Coordinates.Y > highestY {
            highestY = location.Coordinates.Y
        }
    }

    topLeft = Coordinates{
        X: lowestX,
        Y: lowestY,
    }
    bottomRight = Coordinates{
        X: highestX,
        Y: highestY,
    }

    return
}

func extractPointsFromInput(input string) (points []*Point) {
    pointStrings := strings.Split(input, "\n")
    compiledPattern, err := regexp.Compile(PointPattern)
    if err != nil {
        panic(err.Error())
    }

    points = make([]*Point, 0, len(pointStrings))
    for _, pointString := range pointStrings {
        matches := compiledPattern.FindStringSubmatch(pointString)
        x, err := strconv.ParseInt(matches[1], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        y, err := strconv.ParseInt(matches[2], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        velocityX, err := strconv.ParseInt(matches[3], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        velocityY, err := strconv.ParseInt(matches[4], 10, 32)
        if err != nil {
            panic(err.Error())
        }
        points = append(points, &Point{
            Coordinates: Coordinates{
                X: int(x),
                Y: int(y),
            },
            Velocity: Velocity{
                XPerSecond: int(velocityX),
                YPerSecond: int(velocityY),
            },
        })
    }

    return
}

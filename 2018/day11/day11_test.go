package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "testing"
)

const AnswerPart1 = "21, 42"
const AnswerPart2 = "230, 212, 13"

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 11)
    gridSerial := extractGridSerialNumberFromInput(input)
    grid := newFiniteGrid(Coordinates{X: 1, Y: 1}, Coordinates{X: 300, Y: 300}, gridSerial)
    topLeft, _ := findLargestPowerAreaIn(grid, 3, 3)
    if topLeft.String() != AnswerPart1 {
        t.Errorf("expected top left coordinates of largest power in 3 x 3 area to be %s, got %s", AnswerPart1, topLeft)
    }
    topLeft, _, area := findLargestPowerArea(grid)
    answer := topLeft.String() + ", " + strconv.Itoa(area)
    if answer != AnswerPart2 {
        t.Errorf("expected top left coordinates of largest power in any area to be %s, got %s", AnswerPart2, topLeft)
    }
}

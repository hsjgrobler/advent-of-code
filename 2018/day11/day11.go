package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "strconv"
    "sync"
)

type Coordinates struct {
    X int
    Y int
}

func (coordinates Coordinates) String() string {
    return fmt.Sprintf("%d, %d", coordinates.X, coordinates.Y)
}

type FiniteGrid struct {
    SerialNumber int
    CellPowerMap map[Coordinates]int
    TopLeft      Coordinates
    BottomRight  Coordinates
}

type LargestPowerInArea struct {
    Width          int
    Height         int
    TopLeftCell    Coordinates
    TotalAreaPower int
}

func main() {
    input := util.MustReadDayInput(2018, 11)
    gridSerial := extractGridSerialNumberFromInput(input)
    fmt.Println("Grid serial number:", gridSerial)
    grid := newFiniteGrid(Coordinates{X: 1, Y: 1}, Coordinates{X: 300, Y: 300}, gridSerial)
    topLeft, totalAreaPower := findLargestPowerAreaIn(grid, 3, 3)
    fmt.Println("Largest power in 3x3 area starts at", topLeft, "and has a total power of", totalAreaPower)
    topLeft, totalAreaPower, area := findLargestPowerArea(grid)
    fmt.Println("Largest power in any area starts at", topLeft, "and has a total power of", totalAreaPower, "in an area of", area, "x", area)
}

// @todo This is SLOW (~10 mins), find a faster way to do it (maybe cache previously calculated areas)
func findLargestPowerArea(grid FiniteGrid) (topLeftCell Coordinates, totalAreaPower int, area int) {
    results := make(chan LargestPowerInArea, grid.BottomRight.X)
    wg := &sync.WaitGroup{}
    for i := grid.TopLeft.X; i <= grid.BottomRight.X; i++ {
        fmt.Println("Checking area:", i)
        wg.Add(1)
        go AsyncFindLargestPowerIn(results, wg, grid, i, i)
    }

    go func() {
        fmt.Println("Waiting to close results")
        wg.Wait()
        close(results)
    }()

    totalAreaPower = 0
    topLeftCell = grid.TopLeft
    area = grid.TopLeft.X
    for result := range results {
        fmt.Println("Finished area:", result.Width)
        if result.TotalAreaPower > totalAreaPower {
            totalAreaPower = result.TotalAreaPower
            topLeftCell = result.TopLeftCell
            area = result.Width
        }
    }

    return
}

func AsyncFindLargestPowerIn(results chan LargestPowerInArea, wg *sync.WaitGroup, grid FiniteGrid, width int, height int) {
    topLeftCell, totalAreaPower := findLargestPowerAreaIn(grid, width, height)
    results <- LargestPowerInArea{
        Width:          width,
        Height:         height,
        TopLeftCell:    topLeftCell,
        TotalAreaPower: totalAreaPower,
    }
    wg.Done()
}

func findLargestPowerAreaIn(grid FiniteGrid, width int, height int) (topLeftCell Coordinates, totalAreaPower int) {
    totalAreaPower = 0
    topLeftCell = grid.TopLeft
    for x := grid.TopLeft.X; x <= grid.BottomRight.X-width; x++ {
        for y := grid.TopLeft.Y; y <= grid.BottomRight.Y-height; y++ {
            powerArea := 0
            for w := x; w < x+width; w++ {
                for h := y; h < y+height; h++ {
                    power := grid.CellPowerMap[Coordinates{
                        X: w,
                        Y: h,
                    }]
                    powerArea += power
                }
            }
            if powerArea > totalAreaPower {
                totalAreaPower = powerArea
                topLeftCell = Coordinates{X: x, Y: y}
            }
        }
    }

    return
}

func calculatePowerLevel(coordinates Coordinates, gridSerial int) int {
    rackId := coordinates.X + 10
    powerLevel := rackId * coordinates.Y
    powerLevel += gridSerial
    powerLevel *= rackId
    hundredsDigitString := strconv.Itoa(powerLevel / 100)
    hundredsDigitString = string(hundredsDigitString[len(hundredsDigitString)-1])
    hundredsDigit, err := strconv.ParseInt(hundredsDigitString, 10, 32)
    if err != nil {
        panic(err.Error())
    }
    powerLevel = int(hundredsDigit)
    powerLevel -= 5

    return powerLevel
}

func newFiniteGrid(topLeft Coordinates, bottomRight Coordinates, serialNumber int) FiniteGrid {
    coordinatesMap := make(map[Coordinates]int)
    for x := topLeft.X; x <= bottomRight.X; x++ {
        for y := topLeft.Y; y <= bottomRight.Y; y++ {
            currentCoordinates := Coordinates{X: x, Y: y}
            coordinatesMap[currentCoordinates] = calculatePowerLevel(currentCoordinates, serialNumber)
        }
    }
    return FiniteGrid{
        SerialNumber: serialNumber,
        CellPowerMap: coordinatesMap,
        TopLeft:      topLeft,
        BottomRight:  bottomRight,
    }
}

func extractGridSerialNumberFromInput(input string) int {
    serial, err := strconv.ParseInt(input, 10, 32)
    if err != nil {
        panic(err.Error())
    }

    return int(serial)
}

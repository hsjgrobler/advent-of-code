package main

import (
    "fmt"
    "gitlab.com/HendrikG/advent-of-code/util"
    "regexp"
    "strconv"
)

type GameParameters struct {
    PlayerCount          int
    LastMarblePointValue int
}

type Marble struct {
    Number int
    Next   *Marble
    Prev   *Marble
}

const GameParametersPattern = `(\d+) players; last marble is worth (\d+) points`

func main() {
    input := util.MustReadDayInput(2018, 9)
    gameParameters := extractGameParametersFromInput(input)
    playerPoints := gameLoop(gameParameters)
    winningPlayer, winningPoints := findWinningPlayer(playerPoints)
    fmt.Println("Winning player:", winningPlayer, "with", winningPoints, "points")

    gameParameters.LastMarblePointValue *= 100
    playerPoints = gameLoop(gameParameters)
    winningPlayer, winningPoints = findWinningPlayer(playerPoints)
    fmt.Println("100 x Winning player:", winningPlayer, "with", winningPoints, "points")
}

func findWinningPlayer(playerPoints map[int]int) (winningPlayer int, winningPoints int) {
    for player, points := range playerPoints {
        if points > winningPoints {
            winningPoints = points
            winningPlayer = player
        }
    }

    return
}

func gameLoop(parameters GameParameters) map[int]int {
    playerPoints := make(map[int]int, parameters.PlayerCount)
    currentMarble := &Marble{
        Number: 0,
    }
    currentMarble.Next = currentMarble
    currentMarble.Prev = currentMarble
    for points, i := 0, 1; i < parameters.LastMarblePointValue; i++ {
        player := ((i - 1) % parameters.PlayerCount) + 1
        points = playerPoints[player]
        if i%23 == 0 {
            points += i
            marble := currentMarble
            for j := 0; j < 7; j++ {
                marble = marble.Prev
            }
            marble.Next.Prev = marble.Prev
            marble.Prev.Next = marble.Next
            points += marble.Number
            currentMarble = marble.Next

        } else {
            newMarble := &Marble{
                Number: i,
                Next:   currentMarble.Next.Next,
                Prev:   currentMarble.Next,
            }
            currentMarble.Next.Next.Prev = newMarble
            currentMarble.Next.Next = newMarble
            currentMarble = newMarble
        }
        playerPoints[player] = points
    }

    return playerPoints
}

func extractGameParametersFromInput(input string) GameParameters {
    compiledPattern, err := regexp.Compile(GameParametersPattern)
    if err != nil {
        panic(err.Error())
    }

    matches := compiledPattern.FindStringSubmatch(input)
    playerCount, err := strconv.ParseInt(matches[1], 10, 32)
    if err != nil {
        panic(err.Error())
    }
    lastMarblePointValue, err := strconv.ParseInt(matches[2], 10, 32)
    if err != nil {
        panic(err.Error())
    }

    return GameParameters{
        PlayerCount:          int(playerCount),
        LastMarblePointValue: int(lastMarblePointValue),
    }
}

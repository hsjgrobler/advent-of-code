package main

import (
    "gitlab.com/HendrikG/advent-of-code/util"
    "testing"
)

const AnswerPart1 = 437654
const AnswerPart2 = 3689913905

func TestAnswers(t *testing.T) {
    input := util.MustReadDayInput(2018, 9)
    gameParameters := extractGameParametersFromInput(input)

    playerPoints := gameLoop(gameParameters)
    _, winningPoints := findWinningPlayer(playerPoints)
    if winningPoints != AnswerPart1 {
        t.Errorf("expected winning points to be %d, got %d", AnswerPart1, winningPoints)
    }

    gameParameters.LastMarblePointValue *= 100
    playerPoints = gameLoop(gameParameters)
    _, winningPoints = findWinningPlayer(playerPoints)
    if winningPoints != AnswerPart2 {
        t.Errorf("expected 100 x winning points to be %d, got %d", AnswerPart2, winningPoints)
    }
}
